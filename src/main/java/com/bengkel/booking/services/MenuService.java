package com.bengkel.booking.services;

import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.repositories.BookingOrderRepository;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class MenuService {
	private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
	private static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
	private static List<BookingOrder> listAllBookOrders = BookingOrderRepository.getAllBookingOrder();
	private static Scanner input = new Scanner(System.in);
	public static void run() {
		boolean isLooping = true;
		String[] listMenu = {"Login", "Exit"};
		PrintService.printMenu(listMenu, "Aplikasi Booking Bengkel");
		do {
			System.out.print("Pilihan: ");
			int choice = Integer.valueOf(input.nextLine());
			switch(choice){
				case 0:
					System.out.println("Sampai jumpa!");
					System.exit(0);
				case 1:
					login();
					break;
				default :
					System.out.println("Inputan tidak valid, coba lagi!");
			}
		} while (isLooping);
		
	}
	
	public static void login() {
		Customer customer = BengkelService.loginMenu(listAllCustomers);
		mainMenu(customer);
	}
	
	public static void mainMenu(Customer Customer) {
		String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking", "Logout"};
		int menuChoice = 0;
		boolean isLooping = true;
		
		do {
			PrintService.printMenu(listMenu, "Booking Bengkel Menu");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length-1, 0);
			System.out.println(menuChoice);
			
			switch (menuChoice) {
			case 1:
				//panggil fitur Informasi Customer
				BengkelService.showCustomerInfo(Customer);
				break;
			case 2:
				//panggil fitur Booking Bengkel
				BengkelService.bookingBengkel(Customer, listAllItemService);
				break;
			case 3:
				//panggil fitur Top Up Saldo Coin
				BengkelService.topUpSaldoCoin(Customer);
				break;
			case 4:
				//panggil fitur Informasi Booking Order
				BengkelService.showBookOrders(Customer, listAllBookOrders);
				break;
			case 0:
				System.out.println("Logout");
				isLooping = false;
				BengkelService.logout();
				break;
			default:
				System.out.println("Inputan tidak valid, silahkan coba lagi!");
			}
		} while (isLooping);
		
		
	}
	
	//Silahkan tambahkan kodingan untuk keperluan Menu Aplikasi
}
