package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.Motorcyle;
import com.bengkel.booking.models.Vehicle;

public class PrintService {
	
	public static void printMenu(String[] listMenu, String title) {
		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";
		
		System.out.printf("%-25s %n", title);
		System.out.println(line);
		
		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			}else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		System.out.println(line);
		System.out.println();
	}
	
	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
	    System.out.format(line);
	    int number = 1;
	    String vehicleType = "";
	    for (Vehicle vehicle : listVehicle) {
	    	if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			}else {
				vehicleType = "Motor";
			}
	    	System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(), vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
	    	number++;
	    }
	    System.out.printf(line);
	}
	
	//Silahkan Tambahkan function print sesuai dengan kebutuhan.
	public static List<ItemService> printService(List<ItemService> listItemServices, Vehicle kendaraan) {
		List<ItemService> availableServices = new ArrayList<ItemService>();
		String formatTable = "| %-2s | %-15s | %-15s | %-15s | %-15s |%n";
		String line = "+----+-----------------+-----------------+-----------------+-----------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Service Id", "Nama Service", "Tipe Kendaraan", "Harga");
	    System.out.format(line);
	    int num = 1;
		for(ItemService services : listItemServices){
			if((kendaraan instanceof Car && services.getVehicleType().equals("Car")) || 
			(kendaraan instanceof Motorcyle && services.getVehicleType().equals("Motorcyle"))){
				availableServices.add(services);
				System.out.format(formatTable, num, services.getServiceId(), services.getServiceName(), services.getVehicleType(), services.getPrice());
				num++;
			}
		}
		System.out.println(line);

		return availableServices;
	}

	public static void printOrders(Customer customer, List<BookingOrder> listBookOrder){
		String formatTable = "| %-15s | %-15s | %-60s | %-15s | %-15s |%n";
		String line = "+-----------------+-----------------+--------------------------------------------------------------+-----------------+-----------------+%n";
		System.out.format(line);
		System.out.format(formatTable, "Booking Id", "Nama Customer", "List Service", "Payment Method", "Total Payment");
		System.out.format(line);
		for(BookingOrder order : listBookOrder){
			if(order.getCustomer().getCustomerId().equals(customer.getCustomerId())){
				// String services = order.getServices().stream()
				// 	.map(ItemService::getServiceName)
				// 	.collect(Collectors.joining(", "));
				System.out.format(formatTable, order.getBookingId(), order.getCustomer().getName(), printServices(order.getServices()), order.getPaymentMethod(), order.getTotalPayment());
			}
		}
		System.out.printf(line);
	}

	public static String printServices(List<ItemService> listItemServices){
        String result = "";
        // Bisa disesuaikan kembali
        for (int i = 0; i < listItemServices.size(); i++) {
            ItemService service = listItemServices.get(i);
            result += i == listItemServices.size() - 1 ? service.getServiceName() : service.getServiceName() + ", ";
        }
        return result;
    }

}
