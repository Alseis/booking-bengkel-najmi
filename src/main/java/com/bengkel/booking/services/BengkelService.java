package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;
import com.bengkel.booking.repositories.BookingOrderRepository;

public class BengkelService {
	
	//Silahkan tambahkan fitur-fitur utama aplikasi disini
	
	//Login
	public static Customer loginMenu(List<Customer> listCustomer) {
		int attemp = 1;

		while(attemp <= 3){
			Scanner input = new Scanner(System.in);
			System.out.println("Masukan ID customer:");
			String id = input.nextLine();
			System.out.println("Masukan password customer:");
			String password = input.nextLine();

			boolean idExist = false;
			for(Customer customer : listCustomer){
				if(customer.getCustomerId().equals(id)){
					idExist = true;
					if(customer.getPassword().equals(password)){
						System.out.println("Login berhasil, selamat datang di aplikasi Booking Bengkel!");
						return customer;
					}else{
						System.out.println("Password yang dimasukan salah");
						break;
					}
				}
			}

			if(!idExist){
				System.out.println("Customer ID tidak ada / salah!");
			}attemp++;
			if(attemp > 3){
				System.out.println("Anda sudah salah input sebanyak 3x. Aplikasi berhenti!");
				System.exit(0);
			}
		}

		return null;
	}
	
	//Info Customer
	public static void showCustomerInfo(Customer customer) {
		String statusCustomer = customer instanceof MemberCustomer ? "Member" : "Non Member";
		double saldoKoin = customer instanceof MemberCustomer ? ((MemberCustomer)customer).getSaldoCoin() : 0;

		System.out.println("\n" + statusCustomer);
		System.out.println("\t\t\tCustomer Profile\n");
		System.out.println("Customer ID:\t\t" + customer.getCustomerId());
		System.out.println("Nama:\t\t\t" + customer.getName());
		System.out.println("Customer status:\t" + statusCustomer);
		System.out.println("Alamat:\t\t\t" + customer.getAddress());
		System.out.println(statusCustomer.equals("Member") ? "Saldo Koin:\t\t" + saldoKoin : "");

		PrintService.printVechicle(customer.getVehicles());
	}
	
	//Booking atau Reservation
	public static void bookingBengkel(Customer customer,List<ItemService> listItemServices) {
		Scanner scanner = new Scanner(System.in);
		Vehicle kendaraan = null;

		PrintService.printVechicle(customer.getVehicles());
		while(true){
			System.out.println("Masukkan id kendaraan : ");
			String kendaraanId = scanner.nextLine();
			for(Vehicle vehicle : customer.getVehicles()){
				if(vehicle.getVehiclesId().equals(kendaraanId)){
					kendaraan = vehicle;
					break;
				}
			}
			if(kendaraan != null){
				break;
			}else{
				System.out.println("Kendaraan tidak ditemukan, silakan coba lagi.");
			}
		}
		
		List<ItemService> availableServices = PrintService.printService(listItemServices, kendaraan);
		
		int minServices = 1;
		int maxServices = customer instanceof MemberCustomer ? 2 : 1;
		String tambah = "";
		String serviceName = "";
		List<ItemService> chosenServices = new ArrayList<ItemService>();
		for(int i=0; i<maxServices; i++){
			while(true){
				if(i >= minServices){
					System.out.println("Apakah anda ingin menambahkan Service Lainnya? (Y/T)");
					tambah = scanner.nextLine();
				}
				if(i >= minServices && tambah.equals("T")){
					break; // skip kalau sudah ada service dan pilih T
				}else{
					System.out.println("Pilih service: ");
					serviceName = scanner.nextLine();
				}
				ItemService chosenService = null;
				for(ItemService service : availableServices){
					if(service.getServiceId().equalsIgnoreCase(serviceName)){
						chosenService = service;
						break;
					}
				}
				if(chosenService != null){
					chosenServices.add(chosenService);
					break;
				}else{
					System.out.println("Service tidak ditemukan, silakan coba lagi.");
				}
			}
		}
		
		double totalCost = 0;
		for(ItemService service : chosenServices){
			totalCost += service.getPrice();
		}
		
		String paymentMethod = "";
		if(customer instanceof MemberCustomer){
			while(true){
				System.out.println("Pilih metode pembayaran (1. Saldo Coin, 2. Cash): ");
				int paymentMethodChoice = scanner.nextInt();
				if(paymentMethodChoice == 1 || paymentMethodChoice == 2){
					paymentMethod = (paymentMethodChoice == 1) ? "Saldo Coin" : "Cash";
					break;
				}else{
					System.out.println("Pilihan tidak valid, silakan coba lagi.");
				}
			}
			if(paymentMethod.equals("Saldo Coin")){
				MemberCustomer memberCustomer = (MemberCustomer) customer;
				double discountedCost = totalCost * 0.9; // Apply discount
				if(memberCustomer.getSaldoCoin() < discountedCost){
					System.out.println("Saldo Coin tidak mencukupi");
					return;
				}
				memberCustomer.setSaldoCoin(memberCustomer.getSaldoCoin() - discountedCost); 
			}
		} else {
			paymentMethod = "Cash";
		}
		
		BookingOrder bookingOrder = new BookingOrder();
		int orderCount = BookingOrderRepository.getAllBookingOrder().size();
		bookingOrder.setBookingId(String.format("BO-%03d", orderCount + 1));
		bookingOrder.setCustomer(customer);
		bookingOrder.setServices(chosenServices);
		bookingOrder.setPaymentMethod(paymentMethod);
		bookingOrder.setTotalServicePrice(totalCost);
		
		bookingOrder.calculatePayment();
		
		BookingOrderRepository.getAllBookingOrder().add(bookingOrder);
		
		System.out.println("Total biaya service: " + bookingOrder.getTotalPayment());
	}
	
	//Top Up Saldo Coin Untuk Member Customer
	public static void topUpSaldoCoin(Customer customer) {
		Scanner input = new Scanner(System.in);
		if(customer instanceof MemberCustomer){
			System.out.println("\t\t\tTop Up Saldo COin");
			System.out.println("Masukan besaran top up:");
			double jumlahTopUp = input.nextDouble();
			MemberCustomer memberCustomer = (MemberCustomer)customer;
			memberCustomer.setSaldoCoin(memberCustomer.getSaldoCoin() + jumlahTopUp);
			System.out.println("Saldo coin anda sekarang: " + memberCustomer.getSaldoCoin());
		}else{
			System.out.println("Fitur ini hanya ada pada member!");
		}
	}

	//Show all book order
	public static void showBookOrders(Customer customers, List<BookingOrder> listBookOrder){
		PrintService.printOrders(customers, listBookOrder);
	}
	
	//Logout
	public static void logout() {
		MenuService.run();
	}
}
